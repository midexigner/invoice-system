import React, {useState } from 'react'
import { useHistory } from "react-router-dom"
import { Button } from '@material-ui/core'
import { useAuth } from "../context/AuthContext"
import Header from '../components/Header'
import Sidebar from '../components/Sidebar'

export const Dashboard = () => {
    const [error, setError] = useState("")
    const {logout } = useAuth()
    const history = useHistory()
    async function handleLogout() {
        setError("")
        try {
          await logout()
          history.push("/login")
        } catch {
          setError("Failed to log out")
        }
      }
    return (
        <div>
          <Header />
          <Sidebar/>
           <div>
           <div className="card">
                <div className="card-body">
                <h2 className="text-center mb-4">Proffile</h2>
            {error && <div className='alert alert-danger'>{error}</div> }
               
            <div className="w-100 text-center mt-2 mb-3">
            <Button color='primary' onClick={handleLogout}>
                Logout
              </Button>
            </div>
            </div>
            </div>
           </div>
        </div>
    )
}
