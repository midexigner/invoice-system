import { Button } from "@material-ui/core";
import React, { useRef,useState } from "react";
import { Link } from "react-router-dom";
import {useAuth} from '../context/AuthContext'

const Register = () => {
  const emailRef = useRef();
  const passwordRef = useRef();
  const passwordConfirmRef = useRef();
  const { signup,currentUser} = useAuth()
  const [error, setError] = useState("")
  const [loading, setLoading] = useState(false)
 

  async function handleSubmit(e){
      e.preventDefault()
      
      if(passwordRef.current.value !== passwordConfirmRef.current.value){
return setError('Passwords do not match')
      }
      try {
        setError("")
        setLoading(true)
        await signup(emailRef.current.value, passwordRef.current.value)
        
      } catch {
        setError("Failed to create an account")
      }
     
     setLoading(false)
  }
  return (
    <div className="container d-flex justify-content-center align-items-center vh-100 ">
      <div className="w-100" style={{ maxWidth: "400px" }}>
        <div className="card">
          <div className="card-body">
            <h2 className="text-center mb-4">Register</h2>
            {currentUser && currentUser.email}
            {error && <div className='alert alert-danger'>{error}</div> }
            <form onSubmit={handleSubmit}>
              {/* being email field */}
              <div className="mb-3" id="email">
                <label htmlFor="email">Email</label>
                <input
                  type="email"
                  name="email"
                  className="form-control"
                  ref={emailRef}
                />
              </div>
              {/* end email field */}
              {/* being password field */}
              <div className="mb-3" id="Password">
                <label htmlFor="password">Password</label>
                <input
                  type="password"
                  name="password"
                  className="form-control"
                  ref={passwordRef}
                />
              </div>
              {/* end passwordConfirm field */}
              {/* being password field */}
              <div className="mb-3" id="passwordConfirm">
                <label htmlFor="passwordConfirm">Password Confirm</label>
                <input
                  type="password"
                  name="passwordConfirm"
                  className="form-control"
                  ref={passwordConfirmRef}
                />
              </div>
              {/* end password field */}
              {/* being form submit */}
              <Button disabled={loading} type="submit" className="btn btn-primary w-100">
                Register
              </Button>
              {/* end form submit */}
            </form>
          </div>
          <div className="w-100 text-center mt-2 mb-3">
            Already have an account? <Link to="/login">Log In</Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Register;
