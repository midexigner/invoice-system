import React, { useRef,useState } from "react";
import { Link  } from "react-router-dom";
import {useAuth} from '../context/AuthContext'
const ForgetPassword = () => {
    const emailRef = useRef()
    const { resetPassword } = useAuth()
    const [error, setError] = useState("")
    const [message, setMessage] = useState("")
    const [loading, setLoading] = useState(false)
  async function handleSubmit(e){
    e.preventDefault()
    try {
        setMessage("")
        setError("")
        setLoading(true)
        await resetPassword(emailRef.current.value)
        setMessage("Check your inbox for further instructions")
      } catch {
        setError("Failed to reset password")
      }
   
   setLoading(false)
}
  return (
    <div className="container d-flex justify-content-center align-items-center vh-100 ">
      <div className="w-100" style={{ maxWidth: "400px" }}>
        <div className="card">
          <div className="card-body">
            <h2 className="text-center mb-4">Login In</h2>
            {error && <div className='alert alert-danger'>{error}</div> }
            {message && <div className='alert alert-success'>{message}</div> }
            <form onSubmit={handleSubmit}>
              {/* being email field */}
              <div className="mb-3" id="email">
                <label htmlFor="email">Email</label>
                <input
                  type="email"
                  name="email"
                  className="form-control"
                  ref={emailRef}
                />
              </div>
              {/* end email field */}
              {/* being form submit */}
              <button type="submit" disabled={loading} className="btn btn-primary w-100">
              Reset Password
              </button>
              {/* end form submit */}
              <div className="w-100 text-center mt-3">
            <Link to="/login">Login</Link>
          </div>
            </form>
          </div>
          <div className="w-100 text-center mt-2 mb-3">
            Need an account? <Link to="/register">Regsiter</Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ForgetPassword;
