import React, { useRef,useState } from "react";
import {useHistory  } from "react-router-dom";
import {useAuth} from '../context/AuthContext'
const Login = () => {
  const emailRef = useRef();
  const passwordRef = useRef();
  const [error, setError] = useState("")
  const [loading, setLoading] = useState(false)
  const { login} = useAuth()
  const history = useHistory()
  async function handleSubmit(e){
    e.preventDefault()
    try {
      setError("")
      setLoading(true)
      await login(emailRef.current.value, passwordRef.current.value)
      history.push("/")
    } catch {
      setError("Failed to create an account")
    }
   
   setLoading(false)
}
  return (
    <div className="container d-flex justify-content-center align-items-center vh-100 ">
      <div className="w-100" style={{ maxWidth: "400px" }}>
        <div className="card">
          <div className="card-body">
            <h2 className="text-center mb-4">Login In</h2>
            {error && <div className='alert alert-danger'>{error}</div> }
            <form onSubmit={handleSubmit}>
              {/* being email field */}
              <div className="mb-3" id="email">
                <label htmlFor="email">Email</label>
                <input
                  type="email"
                  name="email"
                  className="form-control"
                  ref={emailRef}
                />
              </div>
              {/* end email field */}
              {/* being password field */}
              <div className="mb-3" id="Password">
                <label htmlFor="password">Password</label>
                <input
                  type="password"
                  name="password"
                  className="form-control"
                  ref={passwordRef}
                />
              </div>
              {/* end passwordConfirm field */}
              {/* being form submit */}
              <button type="submit" disabled={loading} className="btn btn-primary w-100">
                LogIn
              </button>
              {/* end form submit */}
              <div className="w-100 text-center mt-3">
           {/* <Link to="/forgot-password">Forgot Password?</Link>*/}
          </div>
            </form>
          </div>
          {/*<div className="w-100 text-center mt-2 mb-3">
            Need an account? <Link to="/register">Regsiter</Link>
          </div>*/}
        </div>
      </div>
    </div>
  );
};

export default Login;
